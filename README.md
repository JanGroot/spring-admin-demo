**SPRING BOOT DASHBOARD DEMO**

Based on the talk **Bootiful Dashboards** 

https://www.youtube.com/watch?v=u1QnlAbCFys

I've taken the applications in the demo and containerized them.

**Instruction for use:**
* run: mvn install
* run: docker-compose up -d

The admin dashboard will be available at:
http://localhost:8083/

The microservices dashboard will be available at:
http://localhost:8084/